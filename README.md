## cue_to_ffmpeg

Parse cuesheet of a multi-track single file audio and generate script for transcoding to individual files.

### Usage
```bash
cat cuesheet.cue | cue_to_ffmpeg > myscript.sh
. myscript.sh
```
