use std::io::{self, Read};

use cue::cd;
use cue::cd_text::PTI;
use sanitize_filename_reader_friendly::sanitize;

fn main() -> io::Result<()> {
    env_logger::init();

    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer)?;
    log::debug!("Input: {}", buffer);

    let cd = cd::CD::parse(buffer)?;

    println!("#!/bin/bash");
    for (index, track) in cd.tracks().iter().enumerate() {
        {
            let track_length_string: String;
            log::info!(
                "\"{}\"\t\"{:02} - {}\"\t{}\t{}",
                track.get_filename(),
                index + 1,
                track.get_cdtext().read(PTI::Title).unwrap_or_default(),
                track.get_start() as f64 / 75.0,
                match track.get_length() {
                    -1 => "end",
                    _ => {
                        track_length_string = (track.get_length() as f64 / 75.0).to_string();
                        &track_length_string
                    }
                }
            );
        }
        println!(
            "ffmpeg -ss {} {} -i \"{}\" -c:a flac \"{:02} - {}.flac\" &&",
            track.get_start() as f64 / 75.0,
            match track.get_length() {
                -1 => "".to_string(),
                _ => format!("-t {}", (track.get_length() as f64 / 75.0)),
            },
            track.get_filename(),
            index + 1,
            sanitize(
                &track
                    .get_cdtext()
                    .read(PTI::Title)
                    .unwrap_or_else(|| "".to_string())
            )
        );
    }

    println!("echo \"Done. :)\"");

    Ok(())
}
